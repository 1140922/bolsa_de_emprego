Candidato.create({bilhete_identidade: "1234567",
  curriculum_vitae: "2001 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem massa, porttitor sed nunc a, feugiat lacinia metus, 2005.",
  area_profissional: "Culinária",
  nivel_habilitacoes: "Masters degree",
  habilitacoes_literarias: "Curabitur in nisl lobortis libero ullamcorper semper.",
  situacao_profissional: "Employed",
  experiencia_profissional: "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras ac aliquet neque. Vestibulum semper, magna id fringilla feugiat, lectus leo imperdiet eros, sodales ornare felis neque in mauris. Integer rhoncus quam at diam mollis pretium eu et arcu.",
  utilizador_attributes: {
    nome: "José Mourinho",
    email: "jose@mail.com",
    foto: "",
    morada: "Praça de Mouzinho Albuquerque, n128",
    codigo_postal: "3500-345",
    contactos: "228764542/934578231",
    apresentacao: "Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu. ",
    localidade: "Porto",
    pagina: "www.example.com"}})

Entidade.create({nif: "5051312334",
  actividade_profissional: "World Improver"
    utilizador_attributes: {
      nome: "WireMaze",
      email: "wiremaze@mail.com",
      foto: "",
      morada: "Avenida de França, nº256",
      codigo_postal: "4000-123",
      contactos: "228764542/934578231",
      apresentacao: "Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu. ",
      localidade: "Lionesa",
      pagina: "www.wiremaze.com"}})


{"bilhete_identidade"=>"", "curriculum_vitae"=>"", "area_profissional"=>"", "nivel_habilitacoes"=>"Masters degree", "habilitacoes_literarias"=>"", "situacao_profissional"=>"Employed", "experiencia_profissional"=>"", "utilizador_attributes"=><ActionController::Parameters {"nome"=>"testeCandidato", "email"=>"teste@candidato.com", "password"=>"123456", "password_confirmation"=>"123456", "morada"=>"", "codigo_postal"=>"", "contactos"=>"", "apresentacao"=>"", "localidade"=>"", "pagina"=>""}


user = Utilizador.create({
  nome: "Foobar",
  email: "foo@bar.com",
  foto: "pic.jpg",
  morada: "",
  codigo_postal: "",
  contactos: "",
  apresentacao: "",
  localidade: "",
  pagina: ""})
