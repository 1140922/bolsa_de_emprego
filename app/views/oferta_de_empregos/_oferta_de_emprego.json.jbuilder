json.extract! oferta_de_emprego, :id, :titulo, :descricao, :foto, :empresa, :actividade_profissional, :tipo_contrato, :activa, :salario, :validade_inicio, :validade_fim, :entidade_id, :created_at, :updated_at
json.url oferta_de_emprego_url(oferta_de_emprego, format: :json)
