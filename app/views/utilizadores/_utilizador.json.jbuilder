json.extract! utilizador, :id, :nome, :email, :password_digest, :foto, :morada, :codigo_postal, :contactos, :apresentacao, :localidade, :pagina, :user_profile_id, :user_profile_type, :created_at, :updated_at
json.url utilizador_url(utilizador, format: :json)
