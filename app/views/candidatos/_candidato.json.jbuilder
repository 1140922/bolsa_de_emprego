json.extract! candidato, :id, :bilhete_identidade, :curriculum_vitae, :area_profissional, :nivel_habilitacoes, :habilitacoes_literarias, :situacao_profissional, :experiencia_profissional, :created_at, :updated_at
json.url candidato_url(candidato, format: :json)
