json.extract! entidade, :id, :nif, :actividade_profissional, :created_at, :updated_at
json.url entidade_url(entidade, format: :json)
