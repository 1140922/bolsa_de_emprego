class OfertaDeEmprego < ApplicationRecord
  belongs_to :entidade
  validates :entidade_id, presence: true
  mount_uploader :foto, FotoUploader
end
