class Candidato < ApplicationRecord
  has_one :utilizador, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :utilizador

end
