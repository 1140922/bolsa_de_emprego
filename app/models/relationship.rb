class Relationship < ApplicationRecord
  belongs_to :interessado, class_name: "Utilizador"
  belongs_to :sobre_interesse, class_name: "Utilizador"
  validates :interessado_id, presence: true
  validates :sobre_interesse_id, presence: true
end
