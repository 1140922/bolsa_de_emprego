class Entidade < ApplicationRecord
  has_many :oferta_de_empregos, dependent: :destroy
  has_one :utilizador, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :utilizador


end
