class Utilizador < ApplicationRecord
  has_many :active_relationships, class_name:   "Relationship",
                                  foreign_key:  "interessado_id",
                                  dependent:    :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                  foreign_key:  "sobre_interesse_id",
                                  dependent:    :destroy
  has_many :interessado_em, through: :active_relationships, source: :sobre_interesse
  has_many :interessados, through: :passive_relationships, source: :interessado
  belongs_to :meta, polymorphic: true, optional: true
  attr_accessor :remember_token
  mount_uploader :foto, FotoUploader
  before_save   :downcase_email
  validates     :nome, presence: true, length: { maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                      format: { with: VALID_EMAIL_REGEX },
                      uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  validate  :picture_size

  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def self.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = Utilizador.new_token
    update_attribute(:remember_digest, Utilizador.digest(remember_token))
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  def create_reset_digest
    self.reset_token = Utilizador.new_token
    update_attribute(:reset_digest,  Utilizador.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def criar_interesse(other_user)
    interessado_em << other_user
    other_user.interessados.reload
  end

  def remover_interesse(other_user)
    interessado_em.delete(other_user)
    other_user.interessados.reload
  end

  def interessado_em?(other_user)
    interessado_em.include?(other_user)
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

    def picture_size
      if foto.size > 5.megabytes
        errors.add(:foto, "deve ser menor que 5MB")
      end
    end
end
