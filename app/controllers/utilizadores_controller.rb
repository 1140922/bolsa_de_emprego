class UtilizadoresController < ApplicationController
  before_action :set_utilizador, only: [:show, :edit, :update, :destroy]

  # GET /utilizadores
  # GET /utilizadores.json
  def index
    if current_user.meta_type != "Admin"
      redirect_to root_path
    end
    # @utilizadores = Utilizador.all
  end

  # GET /utilizadores/1
  # GET /utilizadores/1.json
  def show
    if current_user.meta_type != "Admin"
      redirect_to root_path
    end
  end

  # GET /utilizadores/new
  def new
    if current_user.meta_type != "Admin"
      redirect_to root_path
    end
    @utilizador = Utilizador.new
  end

  # GET /utilizadores/1/edit
  def edit
    if current_user.meta_type != "Admin"
      redirect_to root_path
    end
  end

  # POST /utilizadores
  # POST /utilizadores.json
  def create
    @utilizador = Utilizador.new(utilizador_params)

    respond_to do |format|
      if @utilizador.save
        format.html { redirect_to @utilizador, notice: 'Utilizador criado com sucesso.' }
        format.json { render :show, status: :created, location: @utilizador }
      else
        format.html { render :new }
        format.json { render json: @utilizador.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /utilizadores/1
  # PATCH/PUT /utilizadores/1.json
  def update
    respond_to do |format|
      if params[:utilizador].has_key?("password_actual")
        if params[:utilizador][:password] != params[:utilizador][:password_confirmation]
          format.html { redirect_to root_path, notice: 'Nova Password e Confirmação da Password têm de ser iguais.'}
        end
        if !!@utilizador.authenticate(params[:utilizador][:password_actual])
          params[:utilizador].except(:password_actual)
          if @utilizador.update(utilizador_params)
            format.html { redirect_to root_path, notice: 'Password alterada com sucesso.'}
          end
        else
          format.html { redirect_to root_path, notice: 'Password actual errada.' }
        end
      else
        if @utilizador.update(utilizador_params)
          format.html { redirect_to @utilizador, notice: 'Utilizador actualizado com sucesso.' }
          format.json { render :show, status: :ok, location: @utilizador }
        else
          format.html { render :edit }
          format.json { render json: @utilizador.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def alterar_password
    @utilizador = current_user
  end

  # DELETE /utilizadores/1
  # DELETE /utilizadores/1.json
  def destroy
    @utilizador.destroy
    respond_to do |format|
      format.html { redirect_to utilizadores_url, notice: 'Utilizador was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_utilizador
      @utilizador = Utilizador.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def utilizador_params
      params.require(:utilizador).permit(:nome, :email, :password, :password_confirmation,
                                          :foto, :morada, :codigo_postal, :contactos,
                                          :apresentacao, :localidade, :pagina,
                                          :user_profile_id, :user_profile_type)
    end

    def correct_user
      @user = Utilizador.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
