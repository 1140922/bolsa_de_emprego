class StaticPagesController < ApplicationController
  def home
    @ultimas_empresas = Entidade.last(3).reverse
    @ultimos_candidatos = Candidato.last(3).reverse
    @ultimas_ofertas = OfertaDeEmprego.last(3).reverse
  end

  def registo
  end

  def meu_perfil
    if logged_in?
      @cu = current_user
      else
        flash[:danger] = "Por favor efectue login!"
        redirect_back_or login_path
      end
  end

end
