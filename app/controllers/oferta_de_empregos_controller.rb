class OfertaDeEmpregosController < ApplicationController
  before_action :set_oferta_de_emprego, only: [:show, :edit, :update, :destroy]
  before_action :authorized, only: [:edit, :update, :destroy]
  include SessionsHelper

  # GET /oferta_de_empregos
  # GET /oferta_de_empregos.json
  def index
    if logged_in?
      @oferta_de_empregos = OfertaDeEmprego.all.paginate(page: params[:page], per_page: 10)
    else
      flash[:danger] = "Por favor efectue login!"
      redirect_to login_path
    end
  end

  # GET /oferta_de_empregos/1
  # GET /oferta_de_empregos/1.json
  def show
    if !logged_in?
      flash[:danger] = "Por favor efectue login!"
      redirect_to login_path
    end
  end

  # GET /oferta_de_empregos/new
  def new
    @oferta_de_emprego = OfertaDeEmprego.new
  end

  # GET /oferta_de_empregos/1/edit
  def edit
  end

  # POST /oferta_de_empregos
  # POST /oferta_de_empregos.json
  def create
    # oferta_de_emprego_params[:empresa] = current_user.meta.empresa
    @oferta_de_emprego = current_user.meta.oferta_de_empregos.build(oferta_de_emprego_params)
    respond_to do |format|
      if @oferta_de_emprego.save
        format.html { redirect_to @oferta_de_emprego, notice: 'Oferta de emprego was successfully created.' }
        format.json { render :show, status: :created, location: @oferta_de_emprego }
      else
        format.html { render :new }
        format.json { render json: @oferta_de_emprego.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /oferta_de_empregos/1
  # PATCH/PUT /oferta_de_empregos/1.json
  def update
    respond_to do |format|
      if @oferta_de_emprego.update(oferta_de_emprego_params)
        format.html { redirect_to @oferta_de_emprego, notice: 'Oferta de emprego was successfully updated.' }
        format.json { render :show, status: :ok, location: @oferta_de_emprego }
      else
        format.html { render :edit }
        format.json { render json: @oferta_de_emprego.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /oferta_de_empregos/1
  # DELETE /oferta_de_empregos/1.json
  def destroy
    @oferta_de_emprego.destroy
    respond_to do |format|
      format.html { redirect_to oferta_de_empregos_url, notice: 'Oferta de emprego was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def authorized
      if OfertaDeEmprego.find(params[:id]).entidade_id != current_user.meta_id
        flash[:danger] = "Não tem permissões para realizar esta operação."
        redirect_to root_url
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_oferta_de_emprego
      @oferta_de_emprego = OfertaDeEmprego.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def oferta_de_emprego_params
      params.require(:oferta_de_emprego).permit(:titulo, :descricao, :foto, :empresa,
                                    :actividade_profissional, :tipo_contrato, :activa,
                                    :salario, :validade_inicio, :validade_fim, :entidade_id)
    end
end
