class EntidadesController < ApplicationController
  before_action :set_entidade, only: [:show, :edit, :update, :destroy]
  include ApplicationHelper
  include SessionsHelper

  # GET /entidades
  # GET /entidades.json
  def index
    if logged_in?
      @entidades = Entidade.all.paginate(page: params[:page], per_page: 10)
    else
      flash[:danger] = "Por favor efectue login!"
      redirect_to login_path
    end
  end

  # GET /entidades/1
  # GET /entidades/1.json
  def show
    if !logged_in?
      flash[:danger] = "Por favor efectue login!"
      redirect_to login_path
    end
  end

  # GET /entidades/new
  def new
    @entidade = Entidade.new
    @entidade.build_utilizador
  end

  # GET /entidades/1/edit
  def edit
    authorized?("Entidade")
  end

  # POST /entidades
  # POST /entidades.json
  def create
    @entidade = Entidade.new(entidade_params)
    respond_to do |format|
      if @entidade.save
        format.html { redirect_to @entidade, notice: 'Entidade was successfully created.' }
        format.json { render :show, status: :created, location: @entidade }
      else
        format.html { render :new }
        format.json { render json: @entidade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entidades/1
  # PATCH/PUT /entidades/1.json
  def update
    if authorized?("Entidade")
      respond_to do |format|
        if @entidade.update(entidade_params)
          log_in(@entidade.utilizador)
          format.html { redirect_to @entidade, notice: 'Entidade actualizado com sucesso.' }
          format.json { render :show, status: :ok, location: @entidade }
        else
          format.html { render :edit }
          format.json { render json: @entidade.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /entidades/1
  # DELETE /entidades/1.json
  def destroy
    if authorized?("Entidade")
      @entidade.destroy
      respond_to do |format|
        format.html { redirect_to entidades_url, notice: 'Entidade was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entidade
      @entidade = Entidade.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entidade_params
      params.require(:entidade).permit(:nif, :actividade_profissional,
                                        utilizador_attributes:
                                            [:nome, :email, :password, :password_confirmation, :foto,
                                              :morada, :codigo_postal, :contactos,
                                              :apresentacao, :localidade, :pagina, :id])
    end
end
