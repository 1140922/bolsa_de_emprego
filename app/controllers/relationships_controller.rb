class RelationshipsController < ApplicationController
  before_action :logged_in_user

  def create
      @user = Utilizador.find(params[:sobre_interesse_id])
      current_user.criar_interesse(@user)
      redirect_to :back
      respond_to do |format|
        format.html { redirect_to @user }
        format.js
      end
  end

  def destroy
    @user = Relationship.find(params[:id]).sobre_interesse
    current_user.remover_interesse(@user)
    redirect_to :back
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  private

    def logged_in_user
      if !logged_in?
        flash[:danger] = "Por favor efectue login!"
        redirect_to login_path
      end
    end

end
