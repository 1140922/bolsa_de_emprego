module ApplicationHelper

  def authorized?(meta_type)
    if !logged_in?
      flash[:danger] = "Por favor efectue login!"
      redirect_to login_path
    end
    if @cu = current_user
      if (@cu.meta_type == "#{meta_type}" && params[:id] == @cu.meta_id.to_s)
        @meta_type_utilizador = @cu.meta
      else
        flash[:danger] = "Não tem permissões para realizar esta operação."
        redirect_to root_url
      end
    end
  end

  def authorized_to_view_elements?(meta_type)
    if logged_in?
      @cu = current_user
      (@cu.meta_type == "#{meta_type}" && params[:id] == @cu.meta_id.to_s)
    end
  end

end
