# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Candidato.create!({"bilhete_identidade"=>"1234567",
  "curriculum_vitae"=>"2001 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem massa, porttitor sed nunc a, feugiat lacinia metus, 2005.",
  "area_profissional"=>"Culinária",
  "nivel_habilitacoes"=>"Licenciado",
   "habilitacoes_literarias"=>"Licenciatura em acepipes e saladas",
   "situacao_profissional"=>"Empregado",
   "experiencia_profissional"=>"3 anos de experiência.",
   "utilizador_attributes"=> {
     "nome"=>"José Mourinho",
     "email"=>"jose@mail.com",
     "foto"=>open("app/assets/images/10.jpg"),
     "password"=>"password",
     "password_confirmation"=>"password",
     "morada"=>"Praça de Mouzinho Albuquerque, n128",
     "codigo_postal"=>"3500-345",
     "contactos"=>"228764542/934578231",
     "apresentacao"=>"Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu.",
     "localidade"=>"Porto",
     "pagina"=>"www.example.com"}})

# @c = Candidato.first

Candidato.create!({"bilhete_identidade"=>"7654321",
  "curriculum_vitae"=>"2001 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lorem massa, porttitor sed nunc a, feugiat lacinia metus, 2005.",
  "area_profissional"=>"Web Developer",
  "nivel_habilitacoes"=>"Mestrado",
   "habilitacoes_literarias"=>"Curabitur in nisl lobortis libero ullamcorper semper.",
   "situacao_profissional"=>"Desempregado",
   "experiencia_profissional"=>"Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras ac aliquet neque. Vestibulum semper, magna id fringilla feugiat, lectus leo imperdiet eros, sodales ornare felis neque in mauris. Integer rhoncus quam at diam mollis pretium eu et arcu.",
   "utilizador_attributes"=> {
     "nome"=>"António Gomes",
     "email"=>"antonio@mail.com",
     "foto"=> open("app/assets/images/29.jpg"),
     "password"=>"password",
     "password_confirmation"=>"password",
     "morada"=>"Avenida da Républica, n43",
     "codigo_postal"=>"2300-567",
     "contactos"=>"933512323/224578231",
     "apresentacao"=>"Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu.",
     "localidade"=>"Porto",
     "pagina"=>"www.example.com"}})

Entidade.create!({"nif"=>"5051312334",
  "actividade_profissional"=>"World Improver",
   "utilizador_attributes"=> {
     "nome"=>"WireMaze",
     "email"=>"wiremaze@mail.com",
     "foto"=> open("app/assets/images/logo-footer.png"),
     "password"=>"password",
     "password_confirmation"=>"password",
     "morada"=>"Avenida de França, nº256",
     "codigo_postal"=>"4000-123",
     "contactos"=>"228764542/934578231",
     "apresentacao"=>"Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu. ",
     "localidade"=>"Lionesa",
     "pagina"=>"www.wiremaze.com"}})

@wiremaze = Entidade.first
# @c.utilizador.criar_interesse(@e.utilizador)
# @e.utilizador.criar_interesse(@c.utilizador)

Entidade.create!({"nif"=>"5054329874",
  "actividade_profissional"=>"Confeitaria, Padaria",
   "utilizador_attributes"=> {
     "nome"=>"Padarias Santo António",
     "email"=>"stantonio@mail.com",
     "foto"=> open("app/assets/images/pao.jpg"),
     "password"=>"password",
     "password_confirmation"=>"password",
     "morada"=>"Rua de Ramalho Eanes, nº134",
     "codigo_postal"=>"4000-123",
     "contactos"=>"228764542/934578231",
     "apresentacao"=>"Phasellus nisi turpis, varius non convallis quis, tincidunt ac turpis. Vivamus et molestie diam, ut dictum tortor. Aliquam ut sapien non nibh pharetra condimentum vel in magna. Nunc consectetur laoreet erat, sed placerat libero porta ut. Fusce cursus dui felis, eu mattis odio placerat ut. Nulla efficitur molestie sem, id cursus nulla porta eu. ",
     "localidade"=>"Porto",
     "pagina"=>"www.example.com"}})

@padaria = Entidade.last
# @e2.utilizador.criar_interesse(@c.utilizador)

ap = "Designer"
nh = "Licenciado"
sp = "Desempregado"
password = "password"
pagina = "www.example.com"

50.times do |n|
  bi = Faker::Number.number(7)
  cv = Faker::Lorem.paragraph
  hl = Faker::Educator.course
  ep = Faker::Lorem.paragraph

  name = Faker::Name.name
  email = "example-#{n+1}@mail.com"
  foto = Faker::Avatar.image
  morada = Faker::Address.street_address
  cp = Faker::Address.zip_code
  contactos = Faker::Number.number(9)
  apresentacao = Faker::Lorem.paragraph
  localidade = Faker::Address.city

  Candidato.create!({"bilhete_identidade"=>"1234567",
    "curriculum_vitae"=>"#{cv}",
    "area_profissional"=>"#{ap}",
    "nivel_habilitacoes"=>"#{nh}",
     "habilitacoes_literarias"=>"#{hl}",
     "situacao_profissional"=>"#{sp}",
     "experiencia_profissional"=>"#{ep}",
     "utilizador_attributes"=> {
       "nome"=>"#{name}",
       "email"=>"#{email}",
       "foto"=>open("app/assets/images/avatar_placeholder.png"),
       "password"=>"password",
       "password_confirmation"=>"password",
       "morada"=>"#{morada}",
       "codigo_postal"=>"#{cp}",
       "contactos"=>"#{contactos}",
       "apresentacao"=>"#{apresentacao}",
       "localidade"=>"#{localidade}",
       "pagina"=>"www.example.com"}})
end

50.times do |n|
  nif = Faker::Number.number(10)
  ap = Faker::Commerce.department

  name = Faker::Company.name
  email = "example-#{51+n}@mail.com"
  morada = Faker::Address.street_address
  cp = Faker::Address.zip_code
  contactos = Faker::Number.number(9)
  apresentacao = Faker::Lorem.paragraph
  localidade = Faker::Address.city

  Entidade.create!({"nif"=>"#{nif}",
    "actividade_profissional"=>"#{ap}",
     "utilizador_attributes"=> {
       "nome"=>"#{name}",
       "email"=>"#{email}",
       "foto"=> open("app/assets/images/entidade_avatar.png"),
       "password"=>"password",
       "password_confirmation"=>"password",
       "morada"=>"#{morada}",
       "codigo_postal"=>"#{cp}",
       "contactos"=>"#{contactos}",
       "apresentacao"=>"#{apresentacao}",
       "localidade"=>"#{localidade}",
       "pagina"=>"www.example.com"}})
end

candidatos_interessados = Candidato.all[0..3]
candidatos_interessados.each { |c| c.utilizador.criar_interesse(Entidade.first.utilizador)}

entidades_interessadas = Entidade.all[0..3]
entidades_interessadas.each { |e| e.utilizador.criar_interesse(Candidato.first.utilizador)}



@oferta = @wiremaze.oferta_de_empregos.build(titulo: "Oferta para a posição de Suporte",
                                  descricao: "Mollitia in enim. Cupiditate possimus dolore ad animi harum reprehenderit id. Quas earum vero. Saepe qui sunt error quam est facilis. Nobis qui quasi atque.",
                                  foto: open("app/assets/images/oferta.png"),
                                  empresa: "WireMaze",
                                  actividade_profissional: "Soluções informáticas para o governo e sociedade.",
                                  tipo_contrato: "full-time",
                                  activa: true,
                                  salario: "Sim.",
                                  validade_inicio: "17/03/2017",
                                  validade_fim: "01/11/2017")
@oferta.save

3.times do |n|
  titulo = Faker::Lorem.sentence
  desc = Faker::Lorem.paragraph
  actv = Faker::Lorem.sentence


  @oferta = @wiremaze.oferta_de_empregos.build(
    titulo: titulo,
    descricao: desc,
    foto: open("app/assets/images/oferta.png"),
    empresa: "WireMaze",
    actividade_profissional: actv,
    tipo_contrato: "Full-time",
    activa: true,
    salario: "0-9999",
    validade_inicio: "17/03/2017",
    validade_fim: "01/11/2017")
  @oferta.save
end

4.times do |n|
  titulo = Faker::Lorem.sentence
  desc = Faker::Lorem.paragraph
  actv = Faker::Lorem.sentence

  @oferta = @padaria.oferta_de_empregos.build(
    titulo: titulo,
    descricao: desc,
    foto: open("app/assets/images/oferta.png"),
    empresa: "Padarias Santo António",
    actividade_profissional: actv,
    tipo_contrato: "Full-time",
    activa: true,
    salario: "0-9999",
    validade_inicio: "17/03/2017",
    validade_fim: "01/11/2017")
  @oferta.save
end

20.times do |n|
  titulo = Faker::Lorem.sentence
  desc = Faker::Lorem.paragraph
  actv = Faker::Lorem.sentence

  @oferta = Entidade.third.oferta_de_empregos.build(
  titulo: titulo,
  descricao: desc,
  foto: open("app/assets/images/oferta.png"),
  empresa: "Von Inc",
  actividade_profissional: actv,
  tipo_contrato: "Full-time",
  activa: true,
  salario: "0-9999",
  validade_inicio: "17/03/2017",
  validade_fim: "01/11/2017")

  @oferta.save
end
