# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170317110807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "candidatos", force: :cascade do |t|
    t.string   "bilhete_identidade"
    t.text     "curriculum_vitae"
    t.string   "area_profissional"
    t.string   "nivel_habilitacoes"
    t.text     "habilitacoes_literarias"
    t.string   "situacao_profissional"
    t.text     "experiencia_profissional"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "entidades", force: :cascade do |t|
    t.string   "nif"
    t.string   "actividade_profissional"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "oferta_de_empregos", force: :cascade do |t|
    t.string   "titulo"
    t.text     "descricao"
    t.string   "foto"
    t.string   "empresa"
    t.string   "actividade_profissional"
    t.string   "tipo_contrato"
    t.boolean  "activa"
    t.string   "salario"
    t.string   "validade_inicio"
    t.string   "validade_fim"
    t.integer  "entidade_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["entidade_id"], name: "index_oferta_de_empregos_on_entidade_id", using: :btree
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "interessado_id"
    t.integer  "sobre_interesse_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["interessado_id", "sobre_interesse_id"], name: "index_relationships_on_interessado_id_and_sobre_interesse_id", unique: true, using: :btree
    t.index ["interessado_id"], name: "index_relationships_on_interessado_id", using: :btree
    t.index ["sobre_interesse_id"], name: "index_relationships_on_sobre_interesse_id", using: :btree
  end

  create_table "utilizadores", force: :cascade do |t|
    t.string   "nome"
    t.string   "email"
    t.string   "password_digest"
    t.string   "foto"
    t.text     "morada"
    t.string   "codigo_postal"
    t.string   "contactos"
    t.text     "apresentacao"
    t.string   "localidade"
    t.string   "pagina"
    t.string   "meta_type"
    t.integer  "meta_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "remember_digest"
    t.index ["meta_type", "meta_id"], name: "index_utilizadores_on_meta_type_and_meta_id", using: :btree
  end

  add_foreign_key "oferta_de_empregos", "entidades"
end
