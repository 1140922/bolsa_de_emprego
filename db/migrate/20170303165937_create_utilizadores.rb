class CreateUtilizadores < ActiveRecord::Migration[5.0]
  def change
    create_table :utilizadores do |t|
      t.string :nome
      t.string :email
      t.string :password_digest
      t.string :foto
      t.text :morada
      t.string :codigo_postal
      t.string :contactos
      t.text :apresentacao
      t.string :localidade
      t.string :pagina
      t.belongs_to :meta, polymorphic: true, index: true

      t.timestamps
    end
  end
end
