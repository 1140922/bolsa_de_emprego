class AddRememberDigestToUtilizadores < ActiveRecord::Migration[5.0]
  def change
    add_column :utilizadores, :remember_digest, :string
  end
end
