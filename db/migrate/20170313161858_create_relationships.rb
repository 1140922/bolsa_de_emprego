class CreateRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :relationships do |t|
      t.integer :interessado_id
      t.integer :sobre_interesse_id

      t.timestamps
    end
    add_index :relationships, :interessado_id
    add_index :relationships, :sobre_interesse_id
    add_index :relationships, [:interessado_id, :sobre_interesse_id], unique: true
  end
end
