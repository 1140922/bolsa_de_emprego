class CreateOfertaDeEmpregos < ActiveRecord::Migration[5.0]
  def change
    create_table :oferta_de_empregos do |t|
      t.string :titulo
      t.text :descricao
      t.string :foto
      t.string :empresa
      t.string :actividade_profissional
      t.string :tipo_contrato
      t.boolean :activa
      t.string :salario
      t.string :validade_inicio
      t.string :validade_fim
      t.references :entidade, foreign_key: true

      t.timestamps
    end
    add_index :oferta_de_empregos, [:entidade_id, :created_at]
  end
end
