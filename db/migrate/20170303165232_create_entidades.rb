class CreateEntidades < ActiveRecord::Migration[5.0]
  def change
    create_table :entidades do |t|
      t.string :nif
      t.string :actividade_profissional

      t.timestamps
    end
  end
end
