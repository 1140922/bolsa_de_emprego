class CreateCandidatos < ActiveRecord::Migration[5.0]
  def change
    create_table :candidatos do |t|
      t.string :bilhete_identidade
      t.text :curriculum_vitae
      t.string :area_profissional
      t.string :nivel_habilitacoes
      t.text :habilitacoes_literarias
      t.string :situacao_profissional
      t.text :experiencia_profissional

      t.timestamps
    end
  end
end
