Rails.application.routes.draw do
  # get 'sessions/new'

  resources :utilizadores
  resources :relationships, only: [:create, :destroy]
  resources :candidatos
  resources :entidades
  resources :oferta_de_empregos
  root    'static_pages#home'
  get     '/alterar_password',  to: 'utilizadores#alterar_password'
  get     '/meu_perfil',        to: 'static_pages#meu_perfil'
  get     '/registo',           to: 'static_pages#registo'
  get     '/login',             to: 'sessions#new'
  post    '/login',             to: 'sessions#create'
  delete  '/logout',            to: 'sessions#destroy'
end
