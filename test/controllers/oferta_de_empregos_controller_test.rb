require 'test_helper'

class OfertaDeEmpregosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @oferta_de_emprego = oferta_de_empregos(:one)
  end

  test "should get index" do
    get oferta_de_empregos_url
    assert_response :success
  end

  test "should get new" do
    get new_oferta_de_emprego_url
    assert_response :success
  end

  test "should create oferta_de_emprego" do
    assert_difference('OfertaDeEmprego.count') do
      post oferta_de_empregos_url, params: { oferta_de_emprego: { activa: @oferta_de_emprego.activa, actividade_profissional: @oferta_de_emprego.actividade_profissional, descricao: @oferta_de_emprego.descricao, empresa: @oferta_de_emprego.empresa, entidade_id: @oferta_de_emprego.entidade_id, foto: @oferta_de_emprego.foto, salario: @oferta_de_emprego.salario, tipo_contrato: @oferta_de_emprego.tipo_contrato, titulo: @oferta_de_emprego.titulo, validade_fim: @oferta_de_emprego.validade_fim, validade_inicio: @oferta_de_emprego.validade_inicio } }
    end

    assert_redirected_to oferta_de_emprego_url(OfertaDeEmprego.last)
  end

  test "should show oferta_de_emprego" do
    get oferta_de_emprego_url(@oferta_de_emprego)
    assert_response :success
  end

  test "should get edit" do
    get edit_oferta_de_emprego_url(@oferta_de_emprego)
    assert_response :success
  end

  test "should update oferta_de_emprego" do
    patch oferta_de_emprego_url(@oferta_de_emprego), params: { oferta_de_emprego: { activa: @oferta_de_emprego.activa, actividade_profissional: @oferta_de_emprego.actividade_profissional, descricao: @oferta_de_emprego.descricao, empresa: @oferta_de_emprego.empresa, entidade_id: @oferta_de_emprego.entidade_id, foto: @oferta_de_emprego.foto, salario: @oferta_de_emprego.salario, tipo_contrato: @oferta_de_emprego.tipo_contrato, titulo: @oferta_de_emprego.titulo, validade_fim: @oferta_de_emprego.validade_fim, validade_inicio: @oferta_de_emprego.validade_inicio } }
    assert_redirected_to oferta_de_emprego_url(@oferta_de_emprego)
  end

  test "should destroy oferta_de_emprego" do
    assert_difference('OfertaDeEmprego.count', -1) do
      delete oferta_de_emprego_url(@oferta_de_emprego)
    end

    assert_redirected_to oferta_de_empregos_url
  end
end
